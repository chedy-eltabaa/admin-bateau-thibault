export class Product {
  constructor(
    public id?: number,
    public unit?: string,
    public category?: number,
    public name?: string,
    public discount?: number,
    public comments?: string,
    public owner?: string,
    public price?: number,
    public price_on_sale?: number,
    public sale?: boolean,
    public availability?: boolean,
    public quantity_stock?: number,
    public quantity_sold?: number) {
  }
}
