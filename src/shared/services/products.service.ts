import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Product} from '../model/Product';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(environment.url_api + '/products/');
  }

  updateProductPromotion(id: number, promotion: number): Observable<Product>  {
    return this.http.get<Product>(environment.url_api + '/putonsale/' + id + '/' + promotion + '/');
  }

}
