import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../shared/services/products.service';
import {Product} from '../../shared/model/Product';
import {Form, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-details-product',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.css']
})
export class ProductsDetailComponent implements OnInit {
  products: Product[] = null;
  productsPoisson: Product[] = [];
  productsCrustace: Product[] = [];
  productFruitMer: Product[] = [];
  product: Product;
  formArrayPoisson: FormArray = new FormArray([]);
  formArrayCrustace: FormArray = new FormArray([]);
  formArrayFruitMer: FormArray = new FormArray([]);

  constructor(public productService: ProductsService) {
  }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(
      products => {
        this.products = products;
        this.createFormArray();
      },
      error => console.error(error)
    );
  }

  pushFormGroup(formArray: FormArray) {
    const formGroup: FormGroup = new FormGroup({
      addQuantity: new FormControl('', Validators.required),
      addPromotion: new FormControl('', [Validators.min(0), Validators.max(100)])
    });
    formArray.push(formGroup);
  }

  createFormArray() {
    this.products.forEach(product => {
      if (product.category === 0) {
        this.pushFormGroup(this.formArrayPoisson);
        this.productsPoisson.push(product);
      } else if (product.category === 1) {
        this.pushFormGroup(this.formArrayFruitMer);
        this.productFruitMer.push(product);
      } else if (product.category === 2) {
        this.pushFormGroup(this.formArrayCrustace);
        this.productsCrustace.push(product);
      }
    });
  }

  onSubmit(formArray: FormArray): void {
    if (formArray.invalid) {
      return;
    }

  }


}

