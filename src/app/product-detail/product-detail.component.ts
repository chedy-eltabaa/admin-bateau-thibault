import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../shared/services/products.service';
import {Product} from '../../shared/model/Product';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-one-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  products: Product[] = [];
  productSelected: Product = null;
  formGroup: FormGroup;

  constructor(private productService: ProductsService, private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(
      products => {
        this.products = products;
      },
      error => {
        console.error(error);
      }
    );

    this.createForm();

  }

  createForm(): void {
    this.formGroup = this.formBuilder.group({
      promotion: ['', [Validators.min(0), Validators.max(100), Validators.required]]
    })
    ;

  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }
    this.productService.updateProductPromotion(this.productSelected.id, this.controls.promotion.value).subscribe(
      () => {
        this.products.map(product => product.discount = this.controls.promotion.value);
        this.router.navigate(['/home']);
      }, error => console.error(error)
    );
  }

  get controls() {
    return this.formGroup.controls;

  }


}
