import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Route, Router } from '@angular/router';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide: boolean = true;
  loginForm: FormGroup;
  matcher: ErrorStateMatcher = new MyErrorStateMatcher();
  loading: boolean = false;
  emailFormControl: FormControl;
  passwordFormControl: FormControl;
  isConnectionError: boolean = false;
  connectionError: string = '';

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router) {
    if (authService.isConnected()) {
      router.navigate(['admin/home'])
    }
    this.emailFormControl = new FormControl('', [Validators.required]);
    this.passwordFormControl = new FormControl('', [Validators.required]);
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm(): void {
    this.loginForm = this.fb.group({});
    this.loginForm.setControl('email', this.emailFormControl);
    this.loginForm.setControl('password', this.passwordFormControl);
  }

  get controls() {
    return this.loginForm.controls;
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authService.login(this.controls.email.value.toLowerCase(), this.controls.password.value).subscribe(
      response => {
        this.authService.setToken(response);
        this.authService.startRefreshTokenTimer();
        this.loading = false;
        this.router.navigate(['admin/home']);
      },
      error => {
        this.loading = false;
        this.isConnectionError = true;
        console.error(error);
        this.connectionError = 'Mot de passe ou username invalide';
      }
    );
  }
}
