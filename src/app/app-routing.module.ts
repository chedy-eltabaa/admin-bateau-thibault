import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProductsDetailComponent} from './products-detail/products-detail.component';
import {HomeComponent} from './home/home.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {AuthGuard} from './auth/auth.guard';
import {LoginComponent} from './login/login.component';
import {AuthChildGuard} from './auth/auth-child.guard';
import {AuthInterceptor} from './auth/authInterceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {
    path: 'admin',
    canActivate: [AuthGuard],
    canActivateChild: [AuthChildGuard],
    children: [
      {path: 'product', component: ProductsDetailComponent},
      {path: 'home', component: HomeComponent},
      {path: 'products', component: ProductDetailComponent}],
  },
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, AuthChildGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class AppRoutingModule {
}
