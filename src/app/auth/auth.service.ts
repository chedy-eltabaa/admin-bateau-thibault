import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SESSION_STORAGE, StorageService} from 'ngx-webstorage-service';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  TOKEN_KEY = 'token';
  TOKEN_REFRESH_KEY = 'token-refresh';
  private refreshTokenTimeout;

  constructor(private http: HttpClient, @Inject(SESSION_STORAGE) private storage: StorageService) {
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(environment.url_api + '/token/', {username: username, password: password});
  }

  refreshToken(): Observable<any> {
    return this.http.post(environment.url_api + '/token/refresh/', {'refresh': this.getRefreshToken(),});
  }

  isConnected(): boolean {
    return !!this.storage.get(this.TOKEN_KEY);
  }

  setToken(token) {
    this.storage.set(this.TOKEN_KEY, token.access);
    if (token.refresh) {
      this.storage.set(this.TOKEN_REFRESH_KEY, token.refresh);
    }
  }

  /**
   * GET the token readeable by JS or user
   */
  getTokenObject(): { token_type: string, exp: number, jti: string, user_id: number } {
    const token = this.getToken();
    let payload = token.split('.')[1];
    return JSON.parse(window.atob(payload));
  }

  /**
   * Get the token without being readable
   */
  getToken(): string {
    return this.storage.get(this.TOKEN_KEY);
  }

  /**
   * GET refresh token
   */
  getRefreshToken(): string {
    return this.storage.get(this.TOKEN_REFRESH_KEY);
  }

  startRefreshTokenTimer() {
    const jwtToken = this.getTokenObject();
    const expires: Date = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - (60 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(
      token => {
        this.setToken(token);
        this.startRefreshTokenTimer();
        console.log('refresh', token);
      }
    ), timeout);
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  logout() {
    this.stopRefreshTokenTimer();
    this.storage.remove(this.TOKEN_KEY);
    this.storage.remove(this.TOKEN_REFRESH_KEY);
  }
}
